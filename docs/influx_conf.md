
# Access to InfluxDB shell
Execute on manager node:
```shell
$ docker -H tcp://localhost:2375 exec -ti InfluxSrv /usr/bin/influx
```

# Create datas retention default policy to 1h
```sql
CREATE RETENTION POLICY cadvisor_retention ON cadvisor DURATION 1h REPLICATION 1 DEFAULT
```

# Alter datas retention default policy to 2h
```sql
ALTER RETENTION POLICY cadvisor_retention ON cadvisor DURATION 2h DEFAULT
```

# Show datas retention policies
```sql
SHOW RETENTION POLICIES ON cadvisor
```

# Get containers on nodes
```sql
SELECT COUNT(value) FROM cpu_usage_total WHERE time > 1467734754s AND time < 1467734774s AND container_name = '/' GROUP BY machine fill(0)
```
