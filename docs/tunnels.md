Update IP with the one of the current manager

# Tunnel Docker
```
ssh -fN -i ~/.ssh/unine_rsa trampoline@clusterinfo.unineuchatel.ch -L 2375:172.16.0.30:2375
ssh -fN -i ~/.ssh/unine_rsa trampoline@clusterinfo.unineuchatel.ch -L 2380:172.16.0.30:2380
```

# Tunnel Influx
```
ssh -fN -i ~/.ssh/unine_rsa trampoline@clusterinfo.unineuchatel.ch -L 8086:172.16.0.30:8086
ssh -fN -i ~/.ssh/unine_rsa trampoline@clusterinfo.unineuchatel.ch -L 8083:172.16.0.30:8083
```

# Tunnel Redis
```
ssh -fN -i ~/.ssh/unine_rsa trampoline@clusterinfo.unineuchatel.ch -L 6379:172.16.0.30:6379
```

# Tunnel Resque-web
```
ssh -fN -i ~/.ssh/unine_rsa trampoline@clusterinfo.unineuchatel.ch -L 5678:172.16.0.30:5678
```

# All tunnels
```
ssh -fN -i ~/.ssh/unine_rsa trampoline@clusterinfo.unineuchatel.ch -L 2375:172.16.0.30:2375 ;\
ssh -fN -i ~/.ssh/unine_rsa trampoline@clusterinfo.unineuchatel.ch -L 2380:172.16.0.30:2380 ;\
ssh -fN -i ~/.ssh/unine_rsa trampoline@clusterinfo.unineuchatel.ch -L 8086:172.16.0.30:8086 ;\
ssh -fN -i ~/.ssh/unine_rsa trampoline@clusterinfo.unineuchatel.ch -L 8083:172.16.0.30:8083 ;\
ssh -fN -i ~/.ssh/unine_rsa trampoline@clusterinfo.unineuchatel.ch -L 6379:172.16.0.30:6379 ;\
ssh -fN -i ~/.ssh/unine_rsa trampoline@clusterinfo.unineuchatel.ch -L 5678:172.16.0.30:5678
```
