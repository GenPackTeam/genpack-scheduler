# Resque

Genpack scheduler use Resque to schedule its tasks.
Resque uses Redis as backend.
Genpack uses also Redis to store some datas shared between all processes.


## Resque workers

Tasks given to Resque are processed by workers.
So you need workers, unless your tasks will not be done.

Workers are launched when the experiment begins. You can configure number of instances for each worker in file `config/resque_launch.yml`:

```yaml
-
  name: run_container
  queues: run_container
  workers_count: 5
-
  name: stop_container
  queues: stop_container
  workers_count: 5
-
  name: listener
  queues: listeners
  workers_count: 4
-
  name: generation
  queues: generation_cycle
  workers_count: 1
-
  name: migration
  queues: migrate
  workers_count: 5
```


## Resque scheduler

Genpack uses **Resque scheduler** to run its generation cycles, which are creating migrations.

Scheduled tasks are defined in the file `config/resque_schedule.yml`:

```yaml
generation_nursery_cycle:
  cron: "*/1 * * * *"
  class: GenerationCycle
  description: "Run a cycle on generation nursery"
  args: nursery

generation_young_cycle:
  cron: "*/1 * * * *"
  class: GenerationCycle
  description: "Run a cycle on generation young"
  args: young
```

If you want to disable generation migrations, you have only to provide here an empty file.


## Redis server
Redis runs in a container which is automatically created and started by Genpack.

At every time in the experiment, you should see something like this:
```shell
$ docker ps
CONTAINER ID        IMAGE                         COMMAND                  CREATED             STATUS              PORTS                                            NAMES
16f231f88948        genpack/swarm:dumb-strategy   "swarm manage --strat"   2 minutes ago       Up 2 minutes        0.0.0.0:2380->2375/tcp                           swarm-manager
9de0a1c55196        tutum/influxdb:0.10           "/run.sh"                39 hours ago        Up 39 hours         0.0.0.0:8083->8083/tcp, 0.0.0.0:8086->8086/tcp   InfluxSrv
a016609ff271        redis:3.2.1-alpine            "docker-entrypoint.sh"   4 days ago          Up 4 days           0.0.0.0:6379->6379/tcp                           redis
```

## Run Redis CLI bind to the current Redis server
```shell
$ docker run -it --link redis:redis --rm redis redis-cli -h redis -p 6379
redis:6379>
```


## Resque-web

*Resque-web* is a web-UI to monitor actvity on Resque. Its server uses by default the port **5678**.

It has to be run on the **manager**, from the directory `genpack`, using this command:

```shell
ubuntu@manager:~/genpack$ resque-web config/resque-web.rb
```

Now you have to open an SSH tunnel from your machine to the manager, on the used port.
Supposing manager IP is **172.16.0.30**, use this command:

```shell
$ ssh -fN -i ~/.ssh/unine_rsa trampoline@clusterinfo.unineuchatel.ch -L 5678:172.16.0.30:5678
```

Once running and SSH tunnel created, it can by accessed from your web browser on address `localhost:5678`.
