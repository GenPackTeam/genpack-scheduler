require 'yaml'

class Settings
  def self.create_section(name, settings)
    self.class.instance_eval do
      define_method(name) do
        settings
      end

      settings.each do |key, value|
        self.send(name).define_singleton_method(key) do
          value.freeze
        end
      end
    end
  end

  begin
    @@settings = YAML::load_file(File.dirname(__FILE__) + '/../config.yml')
  rescue Exception => e
    raise "Something is wrong with your config.yml file: #{e.message}"
  end

  @@settings.each do |key, value|
    create_section(key, value)
  end
end
