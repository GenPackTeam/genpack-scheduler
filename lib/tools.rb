class Tools
  instance_eval do
    def memory_to_bytes(memory)
      match = memory.match /(((\d|\.)*)\s(Gi|Mi)?B)/

      if match
        (match[2].to_f * case match[4]
          when "Ki"
            1000
          when "Mi"
            1000000
          when "Gi"
            1000000000
          else
            1
          end).to_i
      else
        0
      end
    end
  end
end
