class RunContainer < Job
  @queue = :run_container

  def self.perform(name, cpu, mem, duration)
    logger.info "Run container with args: #{name} - CPU #{cpu} - MEM #{mem}"

    tries = 30
    params = { name: name, cpu: cpu, mem: [mem, 0.005].max, type: 'borg' }
    cpu_qty = params[:cpu].to_i + 1

    params[:Cmd] = "-r fixed -n #{cpu_qty} -m #{(params[:mem] * 1000).to_i}MB -c #{(params[:cpu] * 100 / cpu_qty).to_i}".split

    logger.info params.inspect

    begin
      c = genpack.create(nil, params)

    rescue Docker::Error::ServerError => e
      logger.warn "ServerError while creating container (#{params.inspect}) -> #{e.inspect.colorize(:red)}"
      sleep ((e.inspect.include? 'no resources available to schedule container') ? 15 : Random.rand(10))
      retry

    rescue Docker::Error::ConflictError => e
      logger.warn "ConflictError while creating container (#{params.inspect}) -> #{e.inspect.colorize(:red)}"
      sleep Random.rand(10)
      retry unless (tries -= 1).zero?
      begin
        dc = Docker::Container.get params[:name]
        # c = genpack.create(nil, params, dc)
        c = Container.new(nil, params, dc)
      rescue Docker::Error::NotFoundError => e
        logger.warn "NotFoundError while retrieving container (#{params.inspect}) -> #{e.inspect.colorize(:red)}"
        sleep Random.rand(10)
        retry unless (tries -= 1).zero?
        logger.error "WTF!!! #{e.inspect.colorize(:red)}"
        raise e
      end

    rescue Docker::Error::TimeoutError => e
      logger.warn "TimeoutError while creating container (#{params.inspect}) -> #{e.inspect.colorize(:red)}"
      sleep Random.rand(10)
      retry

    rescue Docker::Error::ServerError
      sleep Random.rand(10)
      retry unless (dc = Docker::Container.get params[:name] rescue nil)
      c = Container.new(nil, params, dc)

    # rescue Docker::Error::NotFoundError
    #   retry unless (tries -= 1).zero?
    end

    stop_timestamp = c.created + duration.to_i
    Resque.enqueue_at(stop_timestamp, StopContainer, name, stop_timestamp)

    logger.info "Running Genpack#start - ID: #{c.id} - params: #{params.inspect}"
    begin
      genpack.start(c, params)

      logger.info "Container started - ID #{c.id}".colorize(:yellow)
    rescue Docker::Error::TimeoutError => e
      logger.warn "Docker::Error::TimeoutError while starting container (#{params.inspect}) -> #{e.inspect.colorize(:red)}"
      sleep Random.rand(10)
      retry unless (tries -= 1).zero?
      raise e
    rescue Docker::Error::NotFoundError => e
      logger.warn "Docker::Error::NotFoundError while starting container (#{params.inspect}) -> #{e.inspect.colorize(:red)}"
      sleep Random.rand(10)
      retry unless (tries -= 1).zero?
      raise e
    rescue NoMethodError
      sleep Random.rand(10)
      dc = Docker::Container.get params[:name]
      c = Container.new(nil, params, dc)
      retry
    end
  end
end
