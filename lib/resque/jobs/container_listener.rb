require 'colorize'

class ContainerListener < Job
  @queue = :listeners

  def self.perform(container_id, type, node, start_time)
    logger.info "Beginning 'ContainerListener' job for container #{container_id}"

    listening = true
    counter = counter_sym(type, node)

    # create counter dynamically if it does not exist
    unless genpack.respond_to?(counter)
      genpack.class.send(:counter, counter)
      logger.info "Create counter #{counter.inspect}: #{genpack.send(counter).value}"
    end

    Thread.new do
      sleep 1
      Redis.current.set container_id, 1
      logger.info "Key #{container_id} set in Redis".colorize(:yellow)
    end

    while listening do
      begin
        Docker::Event.stream do |event|
          next unless event.id == container_id
          logger.info "Event received: #{event.inspect.colorize(:magenta)}"

          case event.status
          when 'start'
            logger.info "Event #{event.status} caught: #{event.to_s.colorize(:magenta)}"
            handle(event)
          when 'die'
            logger.info "Event #{event.status} caught: #{event.to_s.colorize(:magenta)}"
            handle(event)
            listening = false
            break
          end
        end
      rescue Docker::Error::TimeoutError, Excon::Errors::SocketError
        logger.warn "Stream TimeoutError, restart stream for container ID #{container_id}".colorize(:red)
      end
    end
  end

  private
    def self.counter_sym(type, node)
      "counter_#{node}_#{type}".gsub('-', '_').to_sym
    end

    def self.handle(event)
      type = event.body["Actor"]["Attributes"]["name"].match(/(\w+)\-\d+/)[1]
      node = event.body["Actor"]["Attributes"]["node.name"]
      counter = counter_sym(type, node)

      case event.status
      when 'start'
        genpack.send(counter).incr
      when 'die'
        genpack.send(counter).decr
      end

      genpack.counters << { time: event.time, node: node, type: type, value: genpack.send(counter).value }
      logger.info "Genpack counters state: #{genpack.counters.values.inspect.colorize(:green)}"
    end
end
