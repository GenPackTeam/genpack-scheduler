class StopListeners < Job
  @queue = :listeners

  def self.perform()
    logger.info "Stopping containers listeners, waiting all pending stop to be done..."
    until Resque.delayed_queue_schedule_size.zero? && Resque.workers.map{ |w| w.job }.select{ |j| j["payload"] && ["RunContainer", "StopContainer", "Migrate"].include?(j["payload"]["class"]) }.empty?
      sleep 10
    end
    logger.info "No pending stop, go for stopping listeners!"

    sleep 30

    logger.info "Stop containers listeners"
    Redis.current.del "genpack:experiment:running"
  end
end
