require 'redis-objects'
require 'colorize'

class Node
  @@logger = Genpack.logger

  attr_accessor :node, :resources, :containers

  def initialize(node:,ip:nil,cpus_total:nil,memory_total:nil)
    @node = node

    @ip = Redis::Value.new(key_for_field 'ip')
    @ip.value = ip if ip

    @resources = Redis::HashKey.new(key_for_field 'resources')
    @resources['cpus_total'] = cpus_total if cpus_total
    @resources['memory_total'] = memory_total if memory_total
    @resources['cpus_reserved'] = 0 unless @resources['cpus_reserved']
    @resources['memory_reserved'] = 0 unless @resources['memory_reserved']

    # @cpus_total = Redis::Value.new('cpus_total', cpus_total)
    # @memory_total = Redis::Value.new('memory_total', memory_total)
    # @containers = Redis::Value.new('containers', containers)

    @counter = Redis::Counter.new(key_for_field 'counter')
    @containers = Redis::List.new(key_for_field 'containers')
  end

  def ip
    @ip.value
  end

  def key_for_field field
    "node:#{node}:#{field}"
  end

  def key_for_container container
    "node:#{node}:container:#{container}"
  end

  def availability
    Math.sqrt(available_resource_ratio('cpus') ** 2 + available_resource_ratio('memory') ** 2)
  end

  def reserve(name:, cpu:, memory:)
    @@logger.debug "Node#reserve node: #{self.inspect}, name: #{name.inspect}, cpu: #{cpu.inspect}, memory: #{memory.inspect}".colorize(:green)

    # persist reserved resources for container
    container_resources = Redis::HashKey.new(key_for_container name)
    container_resources['cpu'] = cpu.to_f
    container_resources['mem'] = memory.to_f * 2 ** 30

    @resources['cpus_reserved'] = @resources['cpus_reserved'].to_f + cpu.to_f
    @resources['memory_reserved'] = @resources['memory_reserved'].to_f + memory.to_f * 2 ** 30

    @counter.incr
    @containers << name

    @@logger.debug "Node status: #{self.inspect}".colorize(:green)
  end

  def release(name:)
    @@logger.debug "Node#release node: #{self.inspect}, name: #{name.inspect}".colorize(:green)

    # get persisted reserved resources for container
    container_resources = Redis::HashKey.new(key_for_container name)
    cpu = container_resources['cpu'].to_f
    memory = container_resources['mem'].to_f

    if cpu.zero? && memory.zero?
      @@logger.debug "Resources for container named #{name.inspect} already released on node #{self.inspect}".colorize(:green)
      return nil
    end

    # reset persisted resources for container
    container_resources['cpu'] = 0
    container_resources['mem'] = 0

    @@logger.debug "Resources to release for container named #{name.inspect} -> cpu: #{cpu.inspect}, memory: #{memory.inspect}".colorize(:green)

    @resources['cpus_reserved'] = @resources['cpus_reserved'].to_f - cpu
    @resources['memory_reserved'] = @resources['memory_reserved'].to_f - memory

    @counter.decr
    @containers.delete name

    @@logger.debug "Node status: #{self.inspect}".colorize(:green)

    self
  end

  def matches(cpu:, memory:)
    @@logger.debug "Node#matches node: #{self.inspect} -> cpu: #{cpu.inspect}, available_resource('cpus'): #{available_resource('cpus')}, memory: #{memory.inspect}, available_resource('memory') #{available_resource('memory')}".colorize(:green)
    result = (available_resource('cpus') >= cpu.to_f) && (available_resource('memory') >= (memory.to_f * 2 ** 30))
    @@logger.debug "Match result: #{result.inspect}".colorize(:green)
    return result
  end

  def reset_resources
    @resources['cpus_reserved'] = 0
    @resources['memory_reserved'] = 0
    @counter.reset
    @containers.clear
  end

  def containers(status: nil, upto: nil)
    filters = {}
    filters[:status] = [status].flatten if status
    # filter 'label' not implemented in Swarm
    # filters[:label] = ["com.docker.swarm.constraints=[\"generation==#{generation}\"]"] if generation

    docker_containers = Docker::Container.all({ all: true, filters: filters.to_json }, self.docker_connection )

    docker_containers.map do |dc|
      next nil if dc.info["Image"] == Genpack::SWARM_IMAGE # do not consider Swarm containers

      begin
        Container.get(dc.id)
      rescue Container::RedisNotFoundError, Docker::Error::NotFoundError => e
        @@logger.error "Error #{e.inspect} on Node#containers(node: #{self.inspect}, status: #{status.inspect}, upto: #{upto.inspect})\n\t-> with arg dc: #{dc.inspect}\n\t-> when mapping #{docker_containers.inspect}\n\nDocker::Container.get(dc.id): #{(Docker::Container.get(dc.id, {}, self.docker_connection) rescue nil).inspect}".colorize(:red)
        @@logger.error "Trace:\n#{e.backtrace.join("\n")}"
        nil
      end
    end
    .compact
    .select do |container|
      (
        status.nil? || [status].flatten.include?(container.info["State"]["Status"]) rescue false
      ) && (
        upto.nil? || Time.parse(container.info["Created"]).to_i < upto rescue false
      )
    end
  end

  def inspect
    "<Node: @node=\"#{@node}\", @ip=\"#{@ip.value}\", @resources=\"#{@resources.all.inspect}\", @counter=\"#{@counter.value}\", @containers=\"#{@containers.values.inspect}\">"
  end

  def self.release_for_job(job_name)
    Redis.current.keys("node*container:#{job_name}")
    .map{ |k| k.match(/node:(.*):container/)[1] }
    .map{ |node| self.new(node:node).release(name:job_name) && node }
    .compact
  end

  def self.reset_node_resources(node_name)
    self.new(node:node_name).reset_resources
  end

  def self.nodes_in_generation(generation)
    self.get_nodes(generation)
  end

  def self.containers_in_generation(generation:, upto: Time.now)
    self.nodes_in_generation(generation)
      .map do |node|
        node.containers(status: "running", upto: upto.to_i)
      end
      .flatten
  end

  def self.infos
    puts [
      'nursery-node-1',
      'nursery-node-2',
      'nursery-node-3',
      'nursery-node-4',
      'nursery-node-5',
      'young-node-1',
      'young-node-2',
      'young-node-3',
      'young-node-4',
      'tenured-node-1',
      'tenured-node-2',
      'tenured-node-3'
    ].map{ |node|
      self.new(node:node).inspect
    }.join("\n")
  end

  # private
    def available_resource(resource)
      @resources["#{resource}_total"].to_f - @resources["#{resource}_reserved"].to_f
    end

    def available_resource_ratio(resource)
      available_resource(resource) / @resources["#{resource}_total"].to_f
    end

    def docker_connection
      Docker::Connection.new("tcp://#{self.ip}", {})
    end

    def self.get_nodes(generation=nil)
      redis_nodes = Redis::Value.new('genpack:nodes:hashes')

      if redis_nodes.value
        nodes_hashes = JSON.parse(redis_nodes.value)
      else
        nodes_hashes = get_infos
        .select{ |k,v| v.is_a?(Hash) && v.include?("ip") }
        .each do |k,v|
          cpus = v["Reserved CPUs"].match /(\d*)\s\/\s(\d*)/
          cpus_reserved = cpus[1].to_i
          cpus_total = cpus[2].to_i

          memory = v["Reserved Memory"].match /(((\d|\.)*)\s(Gi|Mi|Ki)?B)\s\/\s(((\d|\.)*)\s(Gi|Mi|Ki)?B)/
          memory_reserved = Tools.memory_to_bytes(memory[1])
          memory_total = Tools.memory_to_bytes(memory[5])

          self.new(
            node: k,
            ip: v["ip"],
            cpus_total: cpus_total,
            memory_total: memory_total
          )
        end
        .keys

        redis_nodes.value = nodes_hashes.to_json
      end

      nodes_hashes
      .select{ |node| node.index(generation.to_s) == 0 }
      .map do |node|
        self.new(node: node)
      end
      .sort{ |x,y| x.availability <=> y.availability }
    end

    def self.get_infos
      redis_infos = Redis::Value.new('genpack:nodes:infos')
      return JSON.parse(redis_infos.value) if redis_infos.value

      infos = {}
      docker_infos = Docker.info["DriverStatus"]
      current_node = nil

      docker_infos.each do |k,v|
        case k
        when /\u0008(\w*)/
          infos[$1] = v
        when /\s└\s((\w|\s)*)/
          infos[current_node][$1] = v
        else
          current_node = k
          infos[current_node] = {}
          infos[current_node]['ip'] = v
        end
      end
      redis_infos.value = infos.to_json
      infos
    end
end
