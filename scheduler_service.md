# Scheduler Service

*GenPack* provides a scheduling service working with the legacy standalone *Docker Swarm* scheduler.
It permits to organize a cluster of Docker hosts in 3 subclusters called **generation**: *nursery*, *young*, *tenured*.
When *GenPack* is running, it runs periodacally some cycles of migration from a generation to the next one (*i.e.* from *nursery* to *young*, or from *young* to *tenured*).

Containers are instantiated first on the *nursery* when their resources consumption is monitored.
When a cycle of migration from the *nursery* generation to the *young* one is processed, the containers from the *nursery* generation are migrated to the *young* generation if enough metrics on their resources consumption have been collected.
Then they are packed in an optimal way on a node of the *young* generation.
Containers resources usage is still monitored when they are on the *young* generation.
A cycle of migration from the *young* generation to the *tenured* generation is working in the same way.
Once containers have been migrated on the *tenured* generation, they are not monitored anymore.

In its first implementation, *GenPack* does not provide a real migration process, because no technology manages yet container migration in user space, where the context of execution is migrated with the container.
Instead, a migration means here that a container is stopped and started on another Docker host.
That means that *GenPack* can not handle yet the migration for stateful containers.
A full migration process has to be added in *GenPack* as soon as the research on this topic brings it.

When using *GenPack*, containers have to be instantiated using a constraint to deploy them on the *nursery* generation.
The deployment on this generation is then done according to the strategy used by the *Docker Swarm* manager, *spread* or *binpack*.


## Interface *Docker Swarm* manager

__Input:__ For running a container, a `docker run` command on the *Docker Swarm* manager, with the option `-e constraint:generation==nursery` to target the deployment of the container on the *nursery* generation.

__Output:__ The ID of the instantiated container is returned by the *Docker Swarm* manager.

__Users:__ Application or user launching a containerized application or service.
